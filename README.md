## Prerequisite
The project is build on top of `Gradle` and `Intellij`. 

Please import the project as `Gradle` project in `Intellij` and it should resolve all dependencies automatically

## Assumption
* Requirement is about implementation of server accepting more clients and responding to their request. 
`Netty` is high performance NIO client/server framework which is suitable for this requirement.

* By default single line of message would be received so that String Encoder/Decoder are chosen to parse the incoming message with line separator. 
 However, it can be configured to customize encoder/decoder to cater for different type of message.

* One implementation of `ReferencePriceSource` is based on file system in this class `ReferencePriceFileSource`, which reads securityId and corresponding prices into memory.
And its callback `ReferencePriceSourceCacheListener` would update the cache when getting a ref price.


* Implementation of `QuoteCalculationEngine` is just a simple formula in this case but even it takes longer time to calculate the quote price 
`Netty` should be able to handle it because it is non-blocking and asynchronous.

## Testing
* Proper unit testing coverage is done with `JUnit` and `Mockito`

* Starts `ServerMain` first then `TestClient`. Client would connect to server, send quote and receive the price. You'll see relevant logs like below:
            
```java
Server side:
20:47:03.357 [nioEventLoopGroup-3-2] INFO  com.example.marketmaker.net.QuoteRequestHandler - channel active! subscribed Price Source Listener!
20:47:03.373 [nioEventLoopGroup-3-2] INFO  com.example.marketmaker.net.QuoteRequestHandler - Getting data from channel: 700 BUY 100
20:47:03.373 [nioEventLoopGroup-3-2] DEBUG com.example.marketmaker.quote.ReferencePriceSourceCacheListener - Updating 700 with lastest price: 333.6
20:47:03.374 [nioEventLoopGroup-3-2] INFO  com.example.marketmaker.net.QuoteRequestHandler - Writing data back to channel: 33360.00
```
    
```java
Client side:
20:47:03.344 [nioEventLoopGroup-2-1] INFO  com.example.marketmaker.client.TestClientHandler - Activating and sending...
20:47:03.359 [main] INFO  com.example.marketmaker.client.TestClient - Done writing...
20:47:03.374 [nioEventLoopGroup-2-1] INFO  com.example.marketmaker.client.TestClientHandler - Sent complete!
33360.00
20:47:03.380 [nioEventLoopGroup-2-1] INFO  com.example.marketmaker.client.TestClientHandler - received from server: 33360.00
```