package com.example.marketmaker.net;


import com.example.marketmaker.quote.QuoteCalculationEngine;
import com.example.marketmaker.quote.QuoteCalculationEngineImpl;
import com.example.marketmaker.quote.ReferencePriceSource;
import com.example.marketmaker.quote.ReferencePriceSourceListener;
import io.netty.channel.ChannelHandlerContext;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(JUnitParamsRunner.class)
public class QuoteRequestHandlerTest {
    @Mock
    private ReferencePriceSource priceSource;
    @Mock
    private ReferencePriceSourceListener listener;
    @Mock
    private ChannelHandlerContext ctx;
    private QuoteCalculationEngine engine;

    private QuoteRequestHandler underTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        engine = new QuoteCalculationEngineImpl();
        underTest = new QuoteRequestHandler(engine, priceSource, listener);
    }

    @Test(expected = NullPointerException.class)
    public void given_nullParameters_then_throwException() {
        underTest = new QuoteRequestHandler(null, priceSource, listener);
    }

    @Test
    public void given_activeChannel_then_subscribe() throws Exception {
        underTest.channelActive(ctx);
        verify(priceSource, times(1)).subscribe(listener);
    }

    @Test
    @Parameters({
            "123 BUY",
            "123 BUY xxx",
            "xxx BUY 123",
            "123 test 123"
    })
    public void given_invalidMessage_when_read_then_skip(String msg) {
        underTest.channelRead0(ctx, msg);

        verify(priceSource, never()).get(anyInt());
        verify(ctx, never()).writeAndFlush(anyString());
    }

    @Test
    @Parameters({
            "123 BUY 100, 123, true, 100, 200.00",
            "700 BUY 200, 700, true, 200, 333.22",
            "123 SELL 200, 123, false, 200, 400.00",
            "700 SELL 200, 700, false, 200, 666.00",
    })
    public void given_validMessage_when_read_then_getQuotePrice(String msg, int id, boolean buy, int quantity, double refPrice) {
        when(priceSource.get(id)).thenReturn(refPrice);
        underTest.channelRead0(ctx, msg);

        ArgumentCaptor captor = ArgumentCaptor.forClass(String.class);
        verify(ctx, times(1)).writeAndFlush(captor.capture());
        String result = (String) captor.getValue();
        assertEquals(refPrice * quantity * (buy ? 1 : -1), Double.valueOf(result), 0.001d);
    }

    @Test
    @Parameters({
            "123 BUY 100, 123, 0",
            "123 BUY 100, 123, 0",
    })
    public void given_validMessageButNoRef_when_read_then_getNaN(String msg, int id, double refPrice) {
        when(priceSource.get(id)).thenReturn(refPrice);
        underTest.channelRead0(ctx, msg);

        ArgumentCaptor captor = ArgumentCaptor.forClass(String.class);
        verify(ctx, times(1)).writeAndFlush(captor.capture());
        String result = (String) captor.getValue();
        assertTrue(result.contains("NaN"));
    }
}
