package com.example.marketmaker.quote;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(JUnitParamsRunner.class)
public class ReferencePriceFileSourceTest {

    private final double delta = 0.000001;
    private final String testFileName = "ReferencePrice-test.csv";
    @Mock
    private ReferencePriceSourceListener listener;

    private ReferencePriceFileSource underTest;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({
            "",
            "not-existing-file-name"
    })
    public void given_nullOrEmptyFilename_when_load_then_throwException(String invalidFileName) throws IOException {
        underTest = new ReferencePriceFileSource(invalidFileName);
    }

    @Test
    public void given_validFile_when_get_then_checkPrice() throws IOException {
        underTest = new ReferencePriceFileSource(testFileName);
        underTest.subscribe(listener);

        assertEquals(697.24, underTest.get(700), delta);
        assertEquals(71.51, underTest.get(1), delta);
        assertEquals(58.15, underTest.get(5), delta);
        assertEquals(245.80, underTest.get(388), delta);
        assertEquals(77.30, underTest.get(1299), delta);
        assertEquals(Double.NaN, underTest.get(369), delta);

        verify(listener, times(1)).referencePriceChanged(1, 71.51);
        verify(listener, times(1)).referencePriceChanged(5, 58.15);
        verify(listener, times(1)).referencePriceChanged(700, 697.24);
        verify(listener, times(1)).referencePriceChanged(388, 245.80);
        verify(listener, times(1)).referencePriceChanged(1299, 77.30);
        verify(listener, times(1)).referencePriceChanged(369, Double.NaN);

        Map<Integer, Double> map = (Map<Integer, Double>) Whitebox.getInternalState(underTest, "refPriceCache");

        assertEquals(5, map.size());
    }
}
