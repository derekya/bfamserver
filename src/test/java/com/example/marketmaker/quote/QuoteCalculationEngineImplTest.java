package com.example.marketmaker.quote;

import com.example.marketmaker.quote.pojo.QuotePrice;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(JUnitParamsRunner.class)
public class QuoteCalculationEngineImplTest {

    private QuoteCalculationEngine underTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        underTest = new QuoteCalculationEngineImpl();
    }

    @Test
    @Parameters({
            "0, 10, 100",
            "700, 0, 100",
            "700, NaN, 100",
            "700, 700, 0"
    })
    public void given_invalidInput_when_cal_then_returnNaN(int securityId, double referencePrice, int quantity) {
        double result = underTest.calculateQuotePrice(securityId, referencePrice, true, quantity);

        QuotePrice tempQuote = (QuotePrice) Whitebox.getInternalState(underTest, "tempQuotePrice");
        TObjectDoubleHashMap cache = (TObjectDoubleHashMap) Whitebox.getInternalState(underTest, "quotePriceCache");

        assertEquals(Double.NaN, result, 0.001);
        assertEquals(0, tempQuote.getSecurityId());
        assertEquals(0, tempQuote.getQuantity());
        assertEquals(0.0d, tempQuote.getReferencePrice(), 0.001);
        assertEquals(0, cache.size());

    }

    @Test
    @Parameters({
            "700, 654.12, true,  100, 65412.0",
            "700, 655.85, false, 200, -131170.0",
            "700, 666.45, true,  300, 199935.0",
            "700, 660.00, true,  500, 330000.0",
            "700, 690.11, false, 100, -69011.0",
    })
    public void given_validInput_when_calForFirstTime_then_returnAndCachePrice(int securityId, double referencePrice, boolean buy, int quantity, double expected) {
        double result = underTest.calculateQuotePrice(securityId, referencePrice, buy, quantity);

        QuotePrice tempQuote = (QuotePrice) Whitebox.getInternalState(underTest, "tempQuotePrice");
        TObjectDoubleHashMap cache = (TObjectDoubleHashMap) Whitebox.getInternalState(underTest, "quotePriceCache");

        assertEquals(expected, result, 0.001);
        assertEquals(securityId, tempQuote.getSecurityId());
        assertEquals(quantity, tempQuote.getQuantity());
        assertEquals(referencePrice, tempQuote.getReferencePrice(), 0.001);
        assertEquals(1, cache.size());

    }

    @Test
    @Parameters({
            "700, 654.12, true,  100, 65412.0"
    })
    public void given_validInput_when_calTheSameAgain_then_returnPriceFromCache(int securityId, double referencePrice, boolean buy, int quantity, double expected)  {
        TObjectDoubleHashMap mockCache = mock(TObjectDoubleHashMap.class);
        when(mockCache.containsKey(new QuotePrice(securityId, referencePrice, buy, quantity))).thenReturn(true);
        Whitebox.setInternalState(underTest, "quotePriceCache", mockCache);

        underTest.calculateQuotePrice(securityId, referencePrice, buy, quantity);
        verify(mockCache, times(1)).get(anyObject());
    }
}
