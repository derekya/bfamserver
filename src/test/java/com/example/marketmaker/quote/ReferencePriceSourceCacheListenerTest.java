package com.example.marketmaker.quote;

import gnu.trove.map.hash.TIntDoubleHashMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class ReferencePriceSourceCacheListenerTest {

    private ReferencePriceSourceListener underTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        underTest = new ReferencePriceSourceCacheListener();
    }

    @Test
    @Parameters({
            "0, 123",
            "700, -1",
            "700, NaN"
    })
    public void given_invalidInput_when_priceChange_then_cacheNotUpdated(int securityId, double price) {
        underTest.referencePriceChanged(securityId, price);
        TIntDoubleHashMap refPrice = (TIntDoubleHashMap) Whitebox.getInternalState(underTest, "referencePriceCache");
        assertEquals(0, refPrice.size());
    }

    @Test
    @Parameters({
            "700, 694.3",
            "5, 43.1",
            "0369, 355.12"
    })
    public void given_validInput_when_priceChange_then_cacheUpdated(int securityId, double price) {
        underTest.referencePriceChanged(securityId, price);
        TIntDoubleHashMap refPrice = (TIntDoubleHashMap) Whitebox.getInternalState(underTest, "referencePriceCache");
        assertEquals(1, refPrice.size());
        assertEquals(price, refPrice.get(securityId), 0.001d);
    }

    @Test
    public void given_samePrice_when_priceChange_then_cacheUpdate() {
        int testId = 700;
        double testPrice = 333.24;
        underTest.referencePriceChanged(testId, testPrice);
        double latestPrice = 444.44;
        underTest.referencePriceChanged(testId, latestPrice);

        TIntDoubleHashMap refPrice = (TIntDoubleHashMap) Whitebox.getInternalState(underTest, "referencePriceCache");
        assertEquals(1, refPrice.size());
        assertEquals(latestPrice, refPrice.get(testId), 0.001d);
    }
}
