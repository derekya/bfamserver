package com.example.marketmaker.net;

public class QuoteRequest {

    private final int securityId;
    private final boolean isBuy;
    private final int quantity;

    public QuoteRequest(int securityId, boolean isBuy, int quantity) {
        this.securityId = securityId;
        this.isBuy = isBuy;
        this.quantity = quantity;
    }

    public int getSecurityId() {
        return securityId;
    }

    public boolean isBuy() {
        return isBuy;
    }

    public int getQuantity() {
        return quantity;
    }
}
