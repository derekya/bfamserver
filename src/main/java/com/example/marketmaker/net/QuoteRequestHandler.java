package com.example.marketmaker.net;

import com.example.marketmaker.quote.QuoteCalculationEngine;
import com.example.marketmaker.quote.ReferencePriceSource;
import com.example.marketmaker.quote.ReferencePriceSourceListener;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * QuoteRequestHandler to handle quote request, call CalculationEngine and return price of quote back to client
 */
public class QuoteRequestHandler extends SimpleChannelInboundHandler<String> {

    private static final Logger LOG = LoggerFactory.getLogger(QuoteRequestHandler.class);
    private static final String BUY = "BUY";
    private static final String SELL = "SELL";
    private static final String WHITE_SPACE = " ";

    private final QuoteCalculationEngine calculationEngine;
    private final ReferencePriceSource priceSource;
    private final ReferencePriceSourceListener priceSourceListener;

    public QuoteRequestHandler(QuoteCalculationEngine calculationEngine, ReferencePriceSource priceSource, ReferencePriceSourceListener priceSourceListener) {
        checkNotNull(calculationEngine, "QuoteCalculationEngine is Null");
        checkNotNull(priceSource, "ReferencePriceSource is Null");
        checkNotNull(priceSourceListener, "ReferencePriceSourceListener is Null");

        this.calculationEngine = calculationEngine;
        this.priceSource = priceSource;
        this.priceSourceListener = priceSourceListener;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        LOG.info("channel active! subscribed Price Source Listener!");
        priceSource.subscribe(priceSourceListener);
        super.channelActive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        LOG.error("Exception happens when handling quote request", cause);
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        LOG.info("Getting data from channel: " + msg);
        if (!isIncomingMessageValid(msg)) {
            LOG.error("Incoming message isn't valid: {}. SKip", msg);
            return;
        }

        QuoteRequest quote = parseQuoteRequest(msg);
        double sourcePrice = priceSource.get(quote.getSecurityId());
        double finalPrice = calculationEngine.calculateQuotePrice(quote.getSecurityId(), sourcePrice, quote.isBuy(), quote.getQuantity());
        String finalPriceStr = String.format("%.2f\r\n", finalPrice);
        ctx.writeAndFlush(finalPriceStr);
        LOG.info("Writing data back to channel: " + finalPriceStr);
    }

    private QuoteRequest parseQuoteRequest(String msg) {
        String[] line = msg.split(WHITE_SPACE);
        int symbol = Integer.parseInt(line[0].trim());
        boolean isBuy = line[1].trim().toUpperCase().equals(BUY);
        int quantity = Integer.parseInt(line[2].trim());
        return new QuoteRequest(symbol, isBuy, quantity);
    }

    private boolean isIncomingMessageValid(String msg) {
        if (Strings.isNullOrEmpty(msg)) {
            return false;
        }

        String[] line = msg.split(WHITE_SPACE);
        String errorMsg = "";
        if (line.length != 3) {
            errorMsg = "Wrong data format received from client: " + msg;
        } else if (Ints.tryParse(line[0].trim()) == null || Ints.tryParse(line[2].trim()) == null) {
            errorMsg = "Symbol/Quantity isn't numeric: " + msg;
        } else if (!line[1].trim().equalsIgnoreCase(BUY) && !line[1].trim().equalsIgnoreCase(SELL)) {
            errorMsg = "Invalid signal for quote(not BUY or SELL): " + line[1];
        }

        return (Strings.isNullOrEmpty(errorMsg)) ? true : false;
    }
}
