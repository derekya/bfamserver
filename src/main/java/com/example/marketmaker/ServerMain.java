package com.example.marketmaker;

import com.example.marketmaker.net.QuoteRequestHandler;
import com.example.marketmaker.quote.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerMain {

    private final static int DEFAULT_PORT = 9999;
    private final static String DEFAULT_REFERENCE_PRICE_NAME = "ReferencePrice.csv";
    private final static Logger LOG = LoggerFactory.getLogger(ServerMain.class);

    private final int port;

    public ServerMain(int port) {
        this.port = port;
    }

    public static void main(String[] args) throws Exception {
        int port = args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_PORT;

        new ServerMain(port).start();
    }

    public void start() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        QuoteCalculationEngine engine = new QuoteCalculationEngineImpl();
        ReferencePriceSource priceSource = new ReferencePriceFileSource(DEFAULT_REFERENCE_PRICE_NAME);
        ReferencePriceSourceListener priceSourceListener = new ReferencePriceSourceCacheListener();


        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(new DelimiterBasedFrameDecoder(1024, Delimiters.lineDelimiter()));
                            ch.pipeline().addLast(new StringDecoder());
                            ch.pipeline().addLast(new StringEncoder());
                            ch.pipeline().addLast(new QuoteRequestHandler(engine, priceSource, priceSourceListener));
                        }
                    });

            ChannelFuture future = bootstrap.bind(port).sync();
            LOG.info("Server Started!");
            future.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}
