package com.example.marketmaker.quote;

import gnu.trove.map.hash.TIntDoubleHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * One simple implementation of ReferencePriceSourceListener which just update the latest price in cache
 * it could support more actions if required. For example, call downstream with latest price
 */
public class ReferencePriceSourceCacheListener implements ReferencePriceSourceListener {

    private final static Logger LOG = LoggerFactory.getLogger(ReferencePriceSourceCacheListener.class);

    private TIntDoubleHashMap referencePriceCache = new TIntDoubleHashMap(2048);

    @Override
    public void referencePriceChanged(int securityId, double price) {
        if (securityId <= 0 || price < 0 || Double.isNaN(price)) {
            return;
        }

        LOG.debug("Updating {} with latest price: {} ", securityId, price);
        referencePriceCache.put(securityId, price);
    }
}
