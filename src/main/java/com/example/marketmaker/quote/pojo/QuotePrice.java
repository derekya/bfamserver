package com.example.marketmaker.quote.pojo;

import com.google.common.base.Objects;

public class QuotePrice {
    private int securityId;
    private double referencePrice;
    private boolean buy;
    private int quantity;


    public QuotePrice() {
    }

    public QuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        this.securityId = securityId;
        this.referencePrice = referencePrice;
        this.buy = buy;
        this.quantity = quantity;
    }

    public int getSecurityId() {
        return securityId;
    }

    public void setSecurityId(int securityId) {
        this.securityId = securityId;
    }

    public double getReferencePrice() {
        return referencePrice;
    }

    public void setReferencePrice(double referencePrice) {
        this.referencePrice = referencePrice;
    }

    public boolean isBuy() {
        return buy;
    }

    public void setBuy(boolean buy) {
        this.buy = buy;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuotePrice that = (QuotePrice) o;
        return securityId == that.securityId &&
                Double.compare(that.referencePrice, referencePrice) == 0 &&
                buy == that.buy &&
                quantity == that.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(securityId, referencePrice, buy, quantity);
    }
}
