package com.example.marketmaker.quote;

import com.example.marketmaker.quote.pojo.QuotePrice;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

/**
 * One implementation of QuoteCalculationEngine which uses async calculation and cache the result due to calculation may take a long time to execute
 */
public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    private final static Logger LOG = LoggerFactory.getLogger(QuoteCalculationEngineImpl.class);

    private final QuotePrice tempQuotePrice = new QuotePrice();
    private final TObjectDoubleHashMap<QuotePrice> quotePriceCache = new TObjectDoubleHashMap<>(1024 * 1024);

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        if (!isInputValid(securityId, referencePrice, quantity)) {
            return Double.NaN;
        }

        setTempObj(securityId, referencePrice, buy, quantity);

        if (quotePriceCache.containsKey(tempQuotePrice)) {
            return quotePriceCache.get(tempQuotePrice);
        }

        CompletableFuture<Double> calculatePriceFuture = CompletableFuture
                .supplyAsync(() -> referencePrice * quantity * ((buy) ? 1 : -1))
                .whenComplete((r, e) -> quotePriceCache.put(new QuotePrice(securityId, referencePrice, buy, quantity), r))
                .exceptionally(error -> {
                    LOG.error("Failed to calculate quote price for" + securityId, error);
                    return Double.NaN;
                });
        return calculatePriceFuture.join();
    }

    private boolean isInputValid(int securityId, double referencePrice, int quantity) {
        return securityId > 0 && referencePrice > 0 && quantity > 0;
    }

    private void setTempObj(int securityId, double referencePrice, boolean buy, int quantity) {
        tempQuotePrice.setSecurityId(securityId);
        tempQuotePrice.setReferencePrice(referencePrice);
        tempQuotePrice.setBuy(buy);
        tempQuotePrice.setQuantity(quantity);
    }

}
