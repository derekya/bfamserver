package com.example.marketmaker.quote;

import com.google.common.base.Strings;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import static com.google.common.base.Charsets.UTF_8;
import static java.util.stream.Collectors.toMap;

/**
 * One implementation of ReferencePriceSource based on file
 * Read reference price from file and save it in cache
 */
public class ReferencePriceFileSource implements ReferencePriceSource {

    private ReferencePriceSourceListener sourceListener;

    private final Map<Integer, Double> refPriceCache;

    public ReferencePriceFileSource(String fileName) throws IOException {
        if (Strings.isNullOrEmpty(fileName)) {
            throw new IllegalArgumentException("Empty file name for Reference Price Source");
        }
        refPriceCache = loadReferencePrice(fileName);
    }

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        this.sourceListener = listener;
    }

    @Override
    public double get(int securityId) {
        double refPrice = refPriceCache.getOrDefault(securityId, Double.NaN);
        if (sourceListener != null) {
            sourceListener.referencePriceChanged(securityId, refPrice);
        }
        return refPrice;
    }

    private Map<Integer, Double> loadReferencePrice(String fileName) throws IOException {
        URL url = Resources.getResource(fileName);
        return Resources.readLines(url, UTF_8).stream().map(line -> line.split(","))
                .filter(s -> s.length == 2)
                .collect(toMap(s -> Integer.valueOf(s[0].trim()), s -> Double.valueOf(s[1].trim()), (v1, v2) -> v2));
    }
}
